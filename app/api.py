import json
import os
import requests

connectionString = None

def init() :
    global connectionString
    print("\napi - init\n")
    # Get config
    config = json.loads(os.environ['SERVICE_WEATHERAPI_CONFIG'])
    # Build connectiong string
    connectionString = config[0]["owm_list"][0]["domain"] + config[0]["owm_list"][0]["path"] + config[0]["owm_list"][0]["args"]

def getWeather():
    r = requests.get(connectionString)
    return r.json()

def getInfo() :

    resultJson = getWeather()

    print("Temperature now in " + resultJson['name'] + " is " + str(resultJson['main']['temp']) + " Celsius")
    print("and if you look to the sky you see " + resultJson['weather'][0]['description'] )
