import unittest
import api

class TestApi(unittest.TestCase):

    def test_getWeather(self):
        result = api.getWeather()
        self.assertEqual(result['name'], "Lisbon")

if __name__ == '__main__':
    unittest.main()